import { __dispatchEvent } from '../../lib/utils';

// plugin specification: https://prosemirror.net/docs/ref/#state.Plugin_System
export default class SelectionMenu {
  constructor(editorId, schema, editorView, $menuTemplate) {
    this.$menuBox = $menuTemplate;

    this.init(editorId, schema, editorView);
    this.update(editorView, null);
  }

  init(editorId, schema, editorView) {
    const { nodes, marks } = schema;

    // menu close buttom logic
    // In Firefox still is selected text
    const $closeButton = this.$menuBox.querySelector('a');
    $closeButton.addEventListener('click', () => {
      this.update(editorView);
    });

    // hide menu after unfocus editor
    editorView.dom.parentNode.addEventListener('blur', (event) => {
      if (event.relatedTarget
        && !event.relatedTarget.classList.contains('menu-item') // no menu button
        && !event.relatedTarget.classList.contains('contextual-menu-component')) { // no menu close button
        this.update(editorView);
      }
    });

    // menu items logic
    const $menuItems = this.$menuBox.querySelectorAll('.menu-item');
    // sugar for creating different data-tag-id
    let sugar = 0;

    if ($menuItems) {
      $menuItems.forEach(($menuItem) => {
        // get attributes for marked parts
        const color = $menuItem.getAttribute('data-color');
        const dataCommandName = $menuItem.getAttribute('data-command-name');

        $menuItem.addEventListener('click', (event) => {
          event.preventDefault();

          if (!SelectionMenu.isAdded(marks.marker, editorView.state)) {
            // create unique dataTagId
            const dataTagId = `${this.$menuBox.id}-${sugar}`;
            // add remove button from schema
            SelectionMenu.addRemoveButton(editorView, nodes.removeButton);
            // define command
            const command = pm.toggleMark(marks.marker, { color, dataCommandName, dataTagId });
            // marked selection part
            command(editorView.state, editorView.dispatch, editorView);
            // remember selection
            const markedSelection = editorView.state.selection;
            // set selection empty
            SelectionMenu.closeSelection(editorView);
            // increase sugar
            sugar += 1;
            // create one dispatchable dom element to dispatch from this tool
            const $dispatchNode = SelectionMenu.connectAllSelectedParts(editorView, dataTagId);
            // add removing marker logic
            SelectionMenu.removingMarkerHandler({
              editorId, editorView, markedSelection, $dispatchNode, dataTagId, dataCommandName, marker: marks.marker,
            });
            // dispatch event that editor has changed
            __dispatchEvent(document, 'editor.change', {}, {
              text: {
                id: editorId,
                task: 'add',
                tagData: { tagName: dataCommandName },
                value: $dispatchNode,
                tagColor: color,
              },
            });
          } // nothing will be marked
          else {
            this.update(editorView, null);
          }
        });
      });
    }

    // add to editor
    editorView.dom.parentNode.appendChild(this.$menuBox);
  }

  // calling from editor every time when editor is updated
  // manage showing and hiding menu and menu position
  update(view, lastState) {
    const { state, readOnly } = view;
    const activeElementClasses = document.activeElement.classList;

    // have to be canvas dom node
    if (!state || readOnly || state.selection.empty || !activeElementClasses.contains('canvas')) {
      if (!this.$menuBox.classList.contains('hidden')) {
        this.$menuBox.classList.add('hidden');
      }
      return;
    }

    this.$menuBox.classList.remove('hidden');

    if (!this.$menuBox.offsetParent) {
      if (!this.$menuBox.classList.contains('hidden')) {
        this.$menuBox.classList.add('hidden');
      }
      return;
    }
    // Update the Content state before calculating the position
    // this.contentUpdate(editorView.state);

    const { from, to } = state.selection;

    try {
      const start = view.coordsAtPos(from);
      const end = view.coordsAtPos(to);

      const box = this.$menuBox.getBoundingClientRect();

      const offsetParentBox = this.$menuBox.offsetParent.getBoundingClientRect();
      let left = (((start.left + end.left) / 2) - (box.width / 2) - offsetParentBox.left);

      if (left < 5) {
        left = 5;
      }

      this.$menuBox.style.left = `${left}px`;
      this.$menuBox.style.top = `${start.top - offsetParentBox.top - box.height}px`;
    }
    catch (err) {
      console.error('Menu selection error', err);
    }
  }

  destroy() {
    if (this.$menuBox) {
      this.$menuBox.remove();
    }
  }

  // check if part of selection was marked
  static isAdded(marker, state) {
    const { ranges } = state.selection;
    let has = false;

    for (let i = 0; !has && i < ranges.length; i += 1) {
      const { $from, $to } = ranges[i];
      // https://prosemirror.net/docs/ref/#model.Node.rangeHasMark
      has = state.doc.rangeHasMark($from.pos, $to.pos, marker);
    }

    return has;
  }

  // TODO: do in same transaction with toggleMark
  static addRemoveButton(editorView, removeButton) {
    const { state } = editorView;
    const { ranges } = state.selection;

    const lastPosition = ranges[ranges.length - 1].$to.pos;
    // https://prosemirror.net/docs/ref/#state.EditorState.apply
    // https://prosemirror.net/docs/ref/#transform.Transform.insert
    // https://prosemirror.net/docs/ref/#model.NodeType.create
    const newState = state.apply(state.tr.insert(lastPosition, removeButton.create()));
    // update editor state
    editorView.updateState(newState);
  }

  static closeSelection(editorView) {
    const { state } = editorView;

    // https://prosemirror.net/docs/ref/#state.EditorState.apply
    // https://prosemirror.net/docs/ref/#state.Transaction.setSelection
    // https://prosemirror.net/docs/ref/#state.TextSelection^create
    const newState = state.apply(state.tr.setSelection(pm.TextSelection.create(state.doc, 0, 0)));
    // update editor state
    editorView.updateState(newState);
  }

  static removingMarkerHandler(inputs) {
    const {
      editorId, editorView, markedSelection, $dispatchNode, dataTagId, dataCommandName, marker,
    } = inputs;
    // find remove button at the end of marked part
    const $removeButton = editorView.dom.querySelector(`[data-tag-id=${dataTagId}] > span.node-close`);

    if ($removeButton) {
      $removeButton.addEventListener('click', (event) => {
        event.preventDefault();

        // find all marked parts
        const $markedParts = editorView.dom.querySelectorAll(`span[data-tag-id=${dataTagId}]`);
        const { state } = editorView;
        const { tr } = state;
        // for all ranges
        for (let index = 0; index < $markedParts.length; index += 1) {
          const $markedPart = $markedParts[index];
          // https://prosemirror.net/docs/ref/#view.EditorView.posAtDOM
          const fromPos = editorView.posAtDOM($markedPart);
          const toPos = fromPos + $markedPart.textContent.length + 2;
          const matched = [];
          const step = 0;

          // better option but error
          // tr.removeMark($from.pos, $to.pos, marker);

          // copy pased from Transform.prototype.removeMark
          // https://github.com/ProseMirror/prosemirror-transform/blob/master/src/mark.js
          // https://prosemirror.net/docs/ref/#model.Node.nodesBetween
          tr.doc.nodesBetween(fromPos, toPos, (node, pos) => {
            if (!node.isInline) return;

            let toRemove = null;
            if (marker instanceof pm.MarkType) {
              // https://prosemirror.net/docs/ref/#model.MarkType.isInSet
              const found = marker.isInSet(node.marks);
              if (found) toRemove = [found];
            }
            else if (marker) {
              // https://prosemirror.net/docs/ref/#model.Mark.isInSet
              if (marker.isInSet(node.marks)) toRemove = [marker];
            }
            else {
              toRemove = node.marks;
            }

            if (toRemove && toRemove.length) {
              const end = Math.min(pos + node.nodeSize, toPos);
              for (let i = 0; i < toRemove.length; i += 1) {
                const style = toRemove[i];
                let found;
                for (let j = 0; j < matched.length; j += 1) {
                  const m = matched[j];
                  // https://prosemirror.net/docs/ref/#model.Mark.eq
                  if (m.step === step - 1 && style.eq(matched[j].style)) found = m;
                }
                if (found) {
                  found.to = end;
                  found.step = step;
                }
                else {
                  matched.push({
                    style, from: Math.max(pos, fromPos), to: end, step,
                  });
                }
              }
            }
          });
          // https://prosemirror.net/docs/ref/#transform.Transform.step
          // https://prosemirror.net/docs/ref/#transform.RemoveMarkStep
          matched.forEach((m) => tr.step(new pm.RemoveMarkStep(m.from, m.to, m.style)));
        }

        // remove removeButton and update state
        // https://prosemirror.net/docs/ref/#view.EditorView.posAtDOM
        const fromPostRB = editorView.posAtDOM($removeButton);
        tr.delete(fromPostRB - 1, fromPostRB + 1);

        // update editor state
        const newState = state.apply(tr);
        editorView.updateState(newState);

        // dispatch event that editor has changed
        __dispatchEvent(document, 'editor.change', {}, {
          text: {
            id: editorId,
            task: 'remove',
            tagData: { tagName: dataCommandName },
            value: $dispatchNode,
          },
        });
      });
    }
  }

  // helper function for dispatch event
  // connect all marked parts
  // clone first part and add here text content of other parts
  static connectAllSelectedParts(view, dataTagId) {
    const $nodes = view.dom.querySelectorAll(`span[data-tag-id=${dataTagId}]`);
    if ($nodes.length === 1) return $nodes[0];
    if ($nodes.length > 1) {
      const $finalNode = $nodes[0].cloneNode(true);
      let fullContent = $nodes[0].textContent;
      for (let i = 1; i < $nodes.length; i += 1) {
        fullContent += $nodes[i].textContent;
      }
      $finalNode.innerHTML = fullContent;
      return $finalNode;
    }

    console.error("Error: Didn't find parts of marked part of text.");
    return null;
  }
}
